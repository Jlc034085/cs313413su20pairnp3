package edu.luc.etl.cs313.android.shapes.model;

import java.util.List;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	// TODO entirely your job (except onCircle)

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {
		return f.getShape().accept(this);
	}

	@Override
	public Location onGroup(final Group g) {
		List<? extends Shape> shapes = g.getShapes();
		Shape firstShape  = shapes.get(0);
		Location firstLocation = firstShape.accept(this);
		Rectangle r = (Rectangle)firstLocation.getShape();
		int xMin = firstLocation.getX();
		int yMin = firstLocation.getY();
		int xMax =  xMin + r.getWidth();
		int yMax = yMin + r.getHeight();
		for (Shape shape : shapes) {
			Location location = shape.accept(this);
			 r = (Rectangle)location.getShape();
			 xMin = Math.min(xMin, location.getX());
			 yMin = Math.min(yMin, location.getY());
			 xMax = Math.max(xMax, location.getX() + r.getWidth());
			 yMax = Math.max(yMax, location.getY() + r.getHeight());
		}
		return new Location(xMin, yMin,new Rectangle(xMax - xMin, yMax - yMin));
	}

	@Override
	public Location onLocation(final Location l) {
		Location shapeLocation = l.getShape().accept(this);
		return new Location(
		   l.getX() + shapeLocation.getX(),
				l.getY() + shapeLocation.getY(),
				shapeLocation.getShape()
		);
	}

	@Override
	public Location onRectangle(final Rectangle r) {
		return new Location(0, 0, r);
	}

	@Override
	public Location onStrokeColor(final StrokeColor c) {
		return c.getShape().accept(this);
	}

	@Override
	public Location onOutline(final Outline o) {

		return o.getShape().accept(this);
	}

	@Override
	public Location onPolygon(final Polygon s) {
		List<? extends Point> points = s.getPoints();
		Point firstPoint =points.get(0);
		int xMin = firstPoint.getX();
		int xMax = firstPoint.getX();
		int yMin = firstPoint.getY();
		int yMax =	firstPoint.getY();
		for (Point point : points) {
			xMin = Math.min(xMin, point.getX());
			xMax = Math.max(xMax, point.getX());
			yMin = Math.min(yMin,point.getY());
			yMax = Math.max(yMax,point.getY());
		}
		return new Location(xMin, yMin,new Rectangle(xMax - xMin, yMax - yMin));
	}
}
